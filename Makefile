all: src/main.rs
	cargo build --release

run: src/main.rs
	cargo run --release

clean:
	rm -rf target/
	rm -rf Cargo.lock
